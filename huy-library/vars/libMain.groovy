#!/usr/bin/env groovy

def call(Map config = [:]) {
    // try {
    node {
      environment             = "${config.environment}"
      projectName             = "${config.projectName}"
      app                     = "${config.app}"
      credentialsId           = "jenkins-pull-push"
      gitBranch               = "deploy-${environment}"
        stage('Git Checkout') {
            utils.gitPull("application8485246/${projectName}", "${app}", "${credentialsId}", "${gitBranch}")
        }
        stage('Get Image Version'){
          script {
            sh 'git describe --tags --long'
          }
        }

    }
}
    // }
    // catch (Exception e) {
      // echo "Huy"
    // }  
