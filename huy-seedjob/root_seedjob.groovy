#!/usr/bin/env groovy

@Grab('org.yaml:snakeyaml:1.27')

import org.yaml.snakeyaml.Yaml

def currentDir = new File('.').getCanonicalPath()
println "Current directory: $currentDir"

def yamlFile = new File('./workspace/seedjob/jobs.yaml')
def yaml = new Yaml()
def data = yaml.load(yamlFile.text)

def environments = ['dev', 'sandbox', 'staging', 'rebuild']
def projects = data.projects
environments.each {
  environment ->
    folder("$environment") {
      description("Folder for $environment environment")
    }
  projects.each {
    project ->
      def projectName = project.name
    folder("$environment/$projectName") {
      description("Folder for $projectName project")
    }
    def apps = project.apps
    apps.each {
      app ->
        pipelineJob("$environment/$projectName/$app") {
          authenticationToken('auth-token')
          parameters {
            stringParam("environment", "$environment")
            stringParam("projectName", "$projectName")
            stringParam("app", "$app")
          }
          definition {
            cps {
              script(readFileFromWorkspace('seedjob_script.groovy'))
              sandbox()
            }
          }
          triggers {
              gitlab {
                  secretToken("huy")
                  triggerOnPush(true)
              }
          }
        }
    }
  }
}
