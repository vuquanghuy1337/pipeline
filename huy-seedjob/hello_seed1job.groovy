#!/usr/bin/env groovy

def environments = ['dev', 'sandbox', 'staging', 'rebuild']
environments.each { environment ->
  folder("${environment}") {
    description("Folder for ${environment} environment")
  }
}
