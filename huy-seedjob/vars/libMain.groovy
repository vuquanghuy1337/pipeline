#!/usr/bin/env groovy


def call(Map config = [:]) {
    node {
        stage('HelloWorld') {
            echo config.environment
            echo config.projectName
            echo config.app
        }
    }
}

