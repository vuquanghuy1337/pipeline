#!/usr/bin/env groovy

pipelineJob("_gcloud-login") {
  displayName('_gcloud-login')
  description('Login google cloud by service account')
  keepDependencies(false)
  definition {
    cps {
      script("""
pipeline {
  agent any
  stages {
    stage ('Google Cloud Login') {
      steps {
    sh('gcloud auth activate-service-account --key-file=/var/lib/jenkins/gcloud-jenkins-build-sa.json')
      }
    }
  }
}"""
      )
      sandbox()
  }
  // Disables the job, so that no new builds will be executed until the project is re-enabled.
  disabled(false)
  }
}
