#!/bin/usr/env groovy
@Library('gitlab-huy') _

def config = [environment: "$environment", projectName: "$projectName", app: "$app"]
libMain(config)
